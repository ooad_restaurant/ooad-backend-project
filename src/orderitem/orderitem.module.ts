import { Module } from '@nestjs/common';
import { OrderitemService } from './orderitem.service';
import { OrderitemController } from './orderitem.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Orderitem } from './entities/orderitem.entity';
import { Menu } from 'src/menu/entities/menu.entity';
import { Order } from 'src/order/entities/order.entity';
import { OrderItem } from 'src/order/entities/order-item';

@Module({
  imports: [TypeOrmModule.forFeature([OrderItem, Menu, Order, Orderitem])],
  controllers: [OrderitemController],
  providers: [OrderitemService],
})
export class OrderitemModule {}

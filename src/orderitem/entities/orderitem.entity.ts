// export class orderitem {}
import { Menu } from 'src/menu/entities/menu.entity';
import { Order } from 'src/order/entities/order.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  DeleteDateColumn,
  OneToMany,
  ManyToOne,
} from 'typeorm';

@Entity()
export class Orderitem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  itemqty: number;

  @Column()
  orderitemstatus: string;

  @OneToMany(() => Menu, (menu) => menu.Menu_id)
  menuid: Menu[];

  @ManyToOne(() => Order, (order) => order.orderItems)
  order: Order;

  // @OneToMany(() => Menu, (menu) => menu.Menu_name)
  // menuname: Menu[];

  @CreateDateColumn()
  createdAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';
import { Table } from './entities/table.entity';

@Injectable()
export class TableService {
  constructor(
    @InjectRepository(Table) private tablesRepository: Repository<Table>,
  ) {}
  async create(id: number, createTableDto: CreateTableDto) {
    // const table = await this.tablesRepository.findOneBy({ id });
    // if (!table) {
    //   throw new NotFoundException();
    // }
    // const createTable = { ...table, ...createTableDto };
    return this.tablesRepository.save(createTableDto);
  }

  findAll() {
    return this.tablesRepository.find();
  }

  findOne(id: number) {
    return this.tablesRepository.findOne({ where: { id } });
  }

  async update(id: number, updateTableDto: UpdateTableDto) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }
    const updateTable = { ...table, ...updateTableDto };
    return this.tablesRepository.save(updateTable);
  }

  async remove(id: number) {
    const table = await this.tablesRepository.findOneBy({ id });
    if (!table) {
      throw new NotFoundException();
    }

    return this.tablesRepository.softRemove(table);
  }
}

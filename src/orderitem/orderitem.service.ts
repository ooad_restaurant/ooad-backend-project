import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateOrderitemDto } from './dto/create-orderitem.dto';
import { UpdateOrderitemDto } from './dto/update-orderitem.dto';
import { Orderitem } from './entities/orderitem.entity';
import { Menu } from 'src/menu/entities/menu.entity';
import { Order } from 'src/order/entities/order.entity';
import { OrderItem } from 'src/order/entities/order-item';

@Injectable()
export class OrderitemService {
  constructor(
    @InjectRepository(OrderItem)
    private orderitemRepository: Repository<OrderItem>,
    @InjectRepository(Menu)
    private menuRepository: Repository<Menu>,
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
  ) {}
  async create(createOrderitemDto: CreateOrderitemDto) {
    return this.orderitemRepository.save(createOrderitemDto);
  }

  findAll() {
    return this.orderitemRepository.find({
      relations: ['menu_id', 'order_id'],
    });
  }

  findOne(id: number) {
    return this.orderitemRepository.findOne({
      where: { id: id },
      relations: ['menu_id', 'order_id'],
    });
  }

  async updateAdd(id: number) {
    const tmpqty = await this.orderitemRepository.findOne({
      where: { id: id },
    });
    return this.orderitemRepository.update(
      {
        id: id,
      },
      {
        qty: tmpqty.qty + 1,
      },
    );
  }

  async updateDel(id: number) {
    const tmpqty = await this.orderitemRepository.findOne({
      where: { id: id },
    });
    if (tmpqty.qty === 1) {
      return this.remove(id);
    }
    return this.orderitemRepository.update(
      {
        id: id,
      },
      {
        qty: tmpqty.qty - 1,
      },
    );
  }

  async remove(id: number) {
    const orderitem = await this.orderitemRepository.findOneBy({ id: id });
    return this.orderitemRepository.softRemove(orderitem);
  }
}

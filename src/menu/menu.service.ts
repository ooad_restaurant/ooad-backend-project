import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMenuDto } from './dto/create-menu.dto';
import { UpdateMenuDto } from './dto/update-menu.dto';
import { Menu } from './entities/menu.entity';

@Injectable()
export class MenuService {
  constructor(
    @InjectRepository(Menu) private menuRepository: Repository<Menu>,
  ) {}
  create(createMenuDto: CreateMenuDto) {
    return this.menuRepository.save(createMenuDto);
  }

  findAll() {
    return this.menuRepository.find();
  }

  findOne(Menu_id: number) {
    return this.menuRepository.findOne({ where: { Menu_id } });
  }

  async update(Menu_id: number, updateMenuDto: UpdateMenuDto) {
    const menu = await this.menuRepository.findOneBy({ Menu_id });
    if (!menu) {
      throw new NotFoundException();
    }
    const updateMenu = { ...menu, ...updateMenuDto };
    return this.menuRepository.save(updateMenu);
  }

  async remove(Menu_id: number) {
    const menu = await this.menuRepository.findOneBy({ Menu_id });
    if (!menu) {
      throw new NotFoundException();
    }

    return this.menuRepository.softDelete(menu);
  }
}

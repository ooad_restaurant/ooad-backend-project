import { Module } from '@nestjs/common';
import { OrderService } from './order.service';
import { OrderController } from './order.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employee/entities/employee.entity';
import { Order } from './entities/order.entity';
import { Table } from 'src/table/entities/table.entity';
import { OrderItem } from './entities/order-item';
import { User } from 'src/users/entities/user.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Order, OrderItem, User, Table])],
  controllers: [OrderController],
  providers: [OrderService],
})
export class OrderModule {}

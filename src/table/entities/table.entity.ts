import { Order } from 'src/order/entities/order.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
export class Table {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  Table_Status: boolean;
  @OneToMany(() => Order, (order) => order.Table_id)
  order: Order[];
}

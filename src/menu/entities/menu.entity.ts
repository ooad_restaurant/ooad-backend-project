import { OrderItem } from 'src/order/entities/order-item';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Menu {
  @PrimaryGeneratedColumn()
  Menu_id: number;

  @Column({ length: 60 })
  Menu_name: string;

  @Column({ type: 'float' })
  Menu_price: number;

  @CreateDateColumn()
  createdAt: Date;

  @CreateDateColumn()
  updatedAt: Date;

  @CreateDateColumn()
  deletedAt: Date;

  @Column({
    nullable: true,
    length: '128',
    default: 'no_image.jpeg',
  })
  image: string;
  @OneToMany(() => OrderItem, (orderitem) => orderitem.menu_id)
  orderItems: OrderItem[];
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Account } from './entities/account.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account) private accountsResposity: Repository<Account>,
  ) {}
  create(createAccountDto: CreateAccountDto) {
    return this.accountsResposity.save(createAccountDto);
  }

  findAll() {
    return this.accountsResposity.find();
  }

  findOne(id: number) {
    return this.accountsResposity.findOne({ where: { id } });
  }

  async update(id: number, updateAccountDto: UpdateAccountDto) {
    const accout = await this.accountsResposity.findOneBy({ id });
    if (!accout) {
      throw new NotFoundException();
    }
    const updateAccount = { ...accout, ...updateAccountDto };
    return this.accountsResposity.save(updateAccount);
  }

  async remove(id: number) {
    const accout = await this.accountsResposity.findOneBy({ id });
    if (!accout) {
      throw new NotFoundException();
    }
    return this.accountsResposity.softRemove(accout);
  }
}

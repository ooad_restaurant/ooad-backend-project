import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Order } from './entities/order.entity';
import { Table } from 'src/table/entities/table.entity';
import { Employee } from 'src/employee/entities/employee.entity';
import { table } from 'console';
import { User } from 'src/users/entities/user.entity';
import { OrderItem } from './entities/order-item';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    @InjectRepository(User)
    private usersRespository: Repository<User>,
    @InjectRepository(OrderItem)
    private orderitemsRespository: Repository<OrderItem>,
    @InjectRepository(Table)
    private tableRespository: Repository<Table>,
  ) {}
  create(createOrderDto: CreateOrderDto) {
    return this.ordersRepository.save(createOrderDto);
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['user', 'orderItems', 'Table_id'],
    });
  }

  async findByStatus(Order_status: string) {
    return await this.ordersRepository.find({
      where: { Order_status: Order_status },
      relations: ['user', 'orderItems.menu_id', 'Table_id'],
    });
  }

  async findOne(id: number) {
    return await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['user', 'orderItems.menu_id', 'Table_id'],
    });
  }

  async confirmOrder(id: number) {
    return this.ordersRepository.update(
      { id: id },
      { Order_status: 'cooking' },
    );
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOneBy({ id });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.softRemove(order);
  }
}

import { IsNotEmpty } from 'class-validator';
import { CreateMenuDto } from 'src/menu/dto/create-menu.dto';
import { Menu } from 'src/menu/entities/menu.entity';
import { CreateOrderDto } from 'src/order/dto/create-order.dto';
import { Order } from 'src/order/entities/order.entity';

export class CreateOrderitemDto {
  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  status: string;

  menu_id: CreateMenuDto;

  order_id: CreateOrderDto;
}

import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Menu } from './menu/entities/menu.entity';
import { MenuModule } from './menu/menu.module';
import { Stock } from './stocks/entities/stock.entity';
import { StocksModule } from './stocks/stocks.module';
import { Table } from './table/entities/table.entity';
import { TableModule } from './table/table.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { OrderitemModule } from './orderitem/orderitem.module';
import { LoginModule } from './login/login.module';
import { EmployeeModule } from './employee/employee.module';
import { AccountModule } from './account/account.module';
import { Account } from './account/entities/account.entity';
import { OrderModule } from './order/order.module';
import { Employee } from './employee/entities/employee.entity';
import { Order } from './order/entities/order.entity';
import { MulterModule } from '@nestjs/platform-express';
import { Orderitem } from './orderitem/entities/orderitem.entity';
import { OrderItem } from './order/entities/order-item';

@Module({
  imports: [
    MenuModule,
    StocksModule,
    TableModule,
    UsersModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'db.sqlite',
      entities: [Menu, Stock, Table, User, Account, Employee, Order, OrderItem],
      synchronize: true,
    }),
    TableModule,
    OrderitemModule,
    LoginModule,
    EmployeeModule,
    AccountModule,
    OrderModule,
    MulterModule.register({
      dest: './image',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}

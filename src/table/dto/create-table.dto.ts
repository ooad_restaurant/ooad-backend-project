import { IsNotEmpty, Max } from 'class-validator';

export class CreateTableDto {
  @Max(20)
  id: number;
  @IsNotEmpty()
  Table_Status: boolean;
}

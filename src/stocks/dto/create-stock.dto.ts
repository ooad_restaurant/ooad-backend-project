import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  @Length(3, 64)
  name: string;

  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  unit: string;
}

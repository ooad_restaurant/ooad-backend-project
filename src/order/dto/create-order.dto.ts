import { CreateEmployeeDto } from 'src/employee/dto/create-employee.dto';
import { CreateOrderitemDto } from 'src/orderitem/dto/create-orderitem.dto';
import { CreateTableDto } from 'src/table/dto/create-table.dto';
import { CreateUserDto } from 'src/users/dto/create-user.dto';

export class CreateOrderDto {
  id: number;
  Order_date: number;
  Order_total: number;
  Order_status: string;
  // EM_id: CreateEmployeeDto[];
  Table_id: CreateTableDto;
  user: CreateUserDto;
  orderitem: CreateOrderitemDto[];
}

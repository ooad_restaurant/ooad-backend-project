import { Menu } from 'src/menu/entities/menu.entity';
import { Order } from './order.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  qty: number;
  @Column()
  status: string;
  @ManyToOne(() => Menu, (menu) => menu.orderItems)
  menu_id: Menu; //menu name

  @ManyToOne(() => Order, (order) => order.orderItems)
  order_id: Order; // orderid

  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;
}

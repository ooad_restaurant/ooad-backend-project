import { MinLength, IsNotEmpty, Matches } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  login: string;
  @IsNotEmpty()
  role: string;
  @IsNotEmpty()
  salary: number;
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  password: string;
}

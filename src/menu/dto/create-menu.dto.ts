import { IsString, IsNotEmpty, Length, Min } from 'class-validator';

export class CreateMenuDto {
  id: number;
  @IsNotEmpty()
  Menu_name: string;

  @IsNotEmpty()
  Menu_price: number;

  image = 'image/no_image.jpeg';
}

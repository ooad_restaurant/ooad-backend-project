import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { LoginUserDto } from './dto/login-user.dto';

@Injectable()
export class LoginService {
  constructor(private readonly usersService: UsersService) {}
  async login(login: LoginUserDto) {
    const user = await this.usersService.login(login);
    return user ? true : false;
  }
}

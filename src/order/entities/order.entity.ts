import { Employee } from 'src/employee/entities/employee.entity';
import { Table } from 'src/table/entities/table.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  OneToMany,
  ManyToOne,
  UpdateDateColumn,
  DeleteDateColumn,
} from 'typeorm';
import { OrderItem } from './order-item';
import { User } from 'src/users/entities/user.entity';
import { Orderitem } from 'src/orderitem/entities/orderitem.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;
  @CreateDateColumn()
  Order_date: number;
  @Column({ type: 'float' })
  Order_total: number;
  @Column()
  Order_status: string;

  @ManyToOne(() => User, (user) => user.order)
  user: User; //user id
  @ManyToOne(() => Table, (table) => table.order)
  Table_id: Table; //id
  @CreateDateColumn()
  createdAt: Date;

  @UpdateDateColumn()
  updatedAt: Date;

  @DeleteDateColumn()
  deletedAt: Date;

  @OneToMany(() => OrderItem, (orderitem) => orderitem.order_id)
  orderItems: OrderItem[];
}

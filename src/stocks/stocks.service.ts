import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/stock.entity';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock) private stocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto) {
    return this.stocksRepository.save(createStockDto);
  }

  findAll() {
    return this.stocksRepository.find();
  }

  findOne(id: number) {
    return this.stocksRepository.findOne({ where: { id } });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    const stock = await this.stocksRepository.findOneBy({ id });
    if (!stock) {
      throw new NotFoundException();
    }
    const updateStock = { ...stock, ...updateStockDto };
    return this.stocksRepository.save(updateStock);
  }

  async remove(id: number) {
    const stock = await this.stocksRepository.findOneBy({ id });
    if (!stock) {
      throw new NotFoundException();
    }

    return this.stocksRepository.softRemove(stock);
  }
}
